import cv2 
import numpy as np 
import os
import time
import pandas as pd
import shutil
import PIL
from PIL import Image

#comment for git

def image_processing(dir_to_detect = 'test-short',alpha=0.02,threshold = 70,show=False):

    """
    see https://www.geeksforgeeks.org/background-subtraction-in-an-image-using-concept-of-running-average/
    alpha will determine the number of images used to compute average. alpha=0.02 means 50 images are used to compute average image
    """

    all_images = sorted([os.path.join(root, name)
                for root, dirs, files in os.walk(dir_to_detect)
                for name in files
                if name.endswith((".jpg"))])

    img = cv2.imread(all_images[0])  
    (h,w,c) = img.shape #getting image size from first image
    return_list = []

    # modify the data type 
    # setting to 32-bit floating point 
    averageValue1 = np.float32(img) 

    frames = []


    # loop runs if capturing has been initialized.  
    for image_path in all_images:

        #blank image for storing orignial image, average image and difference one next to the other
        new_frame = img = np.zeros((h,int(w*3),3), dtype=np.uint8)
        
        img = cv2.imread(image_path)  
        new_frame[0:h,0:w]=img #first panel of gif
        # new_frame = Image.fromarray(img)
        

        # using the cv2.accumulateWeighted() function 
        # that updates the running average 
        cv2.accumulateWeighted(img, averageValue1, alpha) 
        
        # converting the matrix elements to absolute values  
        # and converting the result to 8-bit.  
        resultingFrames1 = cv2.convertScaleAbs(averageValue1)
        new_frame[0:h,w:2*w]=resultingFrames1 # second panel of gif

        diff = cv2.subtract(resultingFrames1,img)
        new_frame[0:h,2*w:3*w]=diff # last panel of gif
        # convert to grayscale    
        im_gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
        
        # binarize with threshold
        im_bin = (im_gray > threshold)

        frames.append(Image.fromarray(new_frame))
        
        if show:
            # Show two output windows 
            # the input / original frames window 
            cv2.imshow('InputWindow', img) 
        
            # the window showing output of alpha value 0.02 
            cv2.imshow('averageValue1', resultingFrames1) 
            cv2.imshow('Diff', im_gray)
            

        different_pixels = float(np.sum(im_bin)/(w*h))
        return_list.append(different_pixels)
        # Wait for Esc key to stop the program  
        k = cv2.waitKey(30) & 0xff
        if k == 27:  
            break
    
    cv2.destroyAllWindows() 

    frames[0].save('example.gif', format='GIF',
               append_images=frames[1:], save_all=True, duration=100, loop=0)
    

    return all_images, return_list

def post_processing(image_list,pixel_list,dir,copy_images=True):

    df = pd.DataFrame(list(zip(image_list,pixel_list)),columns=['image_path','prop_pixels'])
    
    # formatting dates. Here, dates are incorporated in image name
    df["temp"]=df["image_path"].str.split("_")
    df.loc[:, 'image_name'] = df.temp.map(lambda x: x[1])
    df.loc[:, 'year'] = df.temp.map(lambda x: x[2])
    df.loc[:, 'month'] = df.temp.map(lambda x: x[3])
    df.loc[:, 'day'] = df.temp.map(lambda x: x[4])
    df.loc[:, 'hour'] = df.temp.map(lambda x: x[5])
    df.loc[:, 'min'] = df.temp.map(lambda x: x[6])
    df.loc[:, 'sec_temp1'] = df.temp.map(lambda x: x[7])
    df["sec_temp2"]=df["sec_temp1"].str.split(".")
    df.loc[:, 'sec'] = df.sec_temp2.map(lambda x: x[0])
    del df['temp']
    del df['sec_temp1']
    del df['sec_temp2']
    df["image_temp"]=df["image_name"].str.split("/")
    df.loc[:, 'image'] = df.image_temp.map(lambda x: x[1])
    del df['image_temp']
    del df['image_name']

    
    df['rolling_mean'] = df['prop_pixels'].rolling(50,center=True).mean()
    df['rolling_sd'] = df['prop_pixels'].rolling(50,center=True).std()
    df['threshold'] = df['rolling_mean'] + 2*df['rolling_sd']
    # print(df)
    
    out_df_dir = 'out_df/' + dir
    df_outname = out_df_dir + '-results.csv'
    os.makedirs(os.path.dirname(df_outname), exist_ok=True)
    df.to_csv(df_outname,index=False) 
    filter = df['prop_pixels']>df['threshold']
    df_selec = df[filter]
    print(df_selec)
    
    if copy_images:
        for row in df_selec.iterrows():
            index,data = row
            src_path = data['image_path']
            dest_path = 'out/' + src_path
            os.makedirs(os.path.dirname(dest_path), exist_ok=True)
            shutil.copyfile(src_path,dest_path)



if __name__ == "__main__":

    # with 'test-short' as your target directory
    for dir in os.listdir('test-short/'):
        dirpath='test-short/'+dir
        t0 = time.time()
        images, pixels = image_processing(dirpath)
        time.sleep(2)
        # post_processing(images,pixels,dirpath)
        print(time.time()-t0)
